﻿using CreditExchange.Syndication.Cibil;
using LendFoundry.Foundation.Persistence.Mongo;
using System.Threading.Tasks;
using LendFoundry.Tenant.Client;
using MongoDB.Driver.Linq;
using MongoDB.Driver;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Bson;

namespace CreditExchange.Cibil.Persistence
{
    public class CibilRepository : MongoRepository<ICibilData, CibilData>, ICibilRepository
    {
        static CibilRepository()
        {
            BsonClassMap.RegisterClassMap<CibilData>(map =>
            {
                map.AutoMap();
                map.MapMember(m => m.ReportDate).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));
                var type = typeof(CibilData);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
        }

        public CibilRepository(ITenantService tenantService, IMongoConfiguration configuration) : base(tenantService, configuration, "cibilData")
        {
            CreateIndexIfNotExists("cibilData",
              Builders<ICibilData>.IndexKeys.Ascending(i => i.EntityType).Ascending(i => i.EntityId));
        }
        public async Task<ICibilData> GetByEntityId(string entityType, string entityId)
        {
            return await Query.FirstOrDefaultAsync(a => a.EntityType == entityType && a.EntityId == entityId);
        }
    }
}
