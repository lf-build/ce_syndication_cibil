﻿using System.IO;
using CreditExchange.Cibil;
using CreditExchange.Cibil.Persistence;
using CreditExchange.Syndication.Cibil;
using CreditExchange.Syndication.Cibil.Proxy;
using LendFoundry.Clients.DecisionEngine;
using LendFoundry.Configuration;
using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Lookup.Client;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;

namespace CreditExchange.Cibil.Api
{
    public class Startup
    {
        public void ConfigureServices (IServiceCollection services)
        {

            services.AddSwaggerGen (c =>
            {
                c.SwaggerDoc ("v1", new Info
                {
                    Version = "v1",
                        Title = "Cibil"
                });
                c.AddSecurityDefinition ("apiKey", new ApiKeyScheme ()
                {
                    Type = "apiKey",
                        Name = "Authorization",
                        Description = "For accessing the API a valid JWT token must be passed in all the queries in the 'Authorization' header. The syntax used in the 'Authorization' header should be Bearer xxxxx.yyyyyy.zzzz",
                        In = "header"
                });
                c.DescribeAllEnumsAsStrings ();
                c.IgnoreObsoleteProperties ();
                c.DescribeStringEnumsInCamelCase ();
                c.IgnoreObsoleteActions ();
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine (basePath, "CreditExchange.Cibil.Api.xml");
                c.IncludeXmlComments (xmlPath);
            });
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor> ();

            // services
            services.AddTokenHandler ();
            services.AddHttpServiceLogging (Settings.ServiceName);
            services.AddTenantTime ();

            // interface implements
            services.AddConfigurationService<CibilConfiguration> (Settings.Configuration.Host, Settings.Configuration.Port, "cibil");
            services.AddEventHub (Settings.EventHub.Host, Settings.EventHub.Port, Settings.Nats, Settings.ServiceName);
            services.AddTenantService (Settings.Tenant.Host, Settings.Tenant.Port);
            services.AddDecisionEngine (Settings.DecisionEngine.Host, Settings.DecisionEngine.Port);
            services.AddSingleton<IMongoConfiguration> (p => new MongoConfiguration (Settings.Mongo.ConnectionString, Settings.Mongo.Database));
            services.AddTransient<ICibilConfiguration> (p =>
            {
                var configuration = p.GetService<IConfigurationService<CibilConfiguration>> ().Get ();
                configuration.ProxyUrl = $"http://{Settings.TlsProxy.Host}:{Settings.TlsProxy.Port}";
                return configuration;
            });
            services.AddLookupService (Settings.LookupService.Host, Settings.LookupService.Port);
            services.AddTransient<IProxyReport, ProxyReport> ();
            services.AddTransient<ICibilSyndicationService, CibilSyndicationService> ();
            services.AddTransient<ICibilReportService, CibilReportService> ();
            services.AddTransient<ICibilRepository, CibilRepository> ();
            services.AddMvc ().AddLendFoundryJsonOptions ();
            services.AddCors ();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline
        /// <summary>
        /// Configure
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        /// <param name="loggerFactory"></param>
        public void Configure (IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            app.UseHealthCheck ();
            app.UseSwagger ();

            app.UseSwaggerUI (c =>
            {
                c.SwaggerEndpoint ("/swagger/v1/swagger.json", "CibilSyndication Service");
            });
            app.UseCors (env);
            app.UseErrorHandling ();
            app.UseRequestLogging ();
            app.UseMvc ();
        }
    }
}