﻿namespace CreditExchange.Syndication.Cibil
{
    public interface ICibilConfiguration
    {
        string Url { get; set; }
        string UserId { get; set; }
        string Password { get; set; }
        string SolutionSetId { get; set; }
        string SolutionSetVersion { get; set; }
        string ExecutionMode { get; set; }
        string EnvironmentType { get; set; }
        string InpEnquiryPurpose { get; set; }
        string InpScoreType { get; set; }
        string IsCirReq { get; set; }
        string IsEverifyReq { get; set; }
        string Authentication { get; set; }
        string ClientID { get; set; }
        string ProxyUrl { get; set; }
        bool UseProxy { get; set; }
        bool IsSecondaryReportRequired { get; set; }
        string ReturnHTMLUrl { get; set; }
        string IDVMemberCode { get; set; }
        string IDVPassword { get; set; }
        bool SkipCibilBureauFlag { get; set; }
        bool SkipDSTuIDVisionFlag { get; set; }
        bool SkipDSTuNtcFlag { get; set; }
        string ConsumerConsentForUIDAIAuthentication { get; set; }

        bool MFIBureauFlag { get; set; }

        bool FormattedReport { get; set; }

        string NTCProductType { get; set; }

        string MFIEnquiryAmount { get; set; }

        string MFILoanPurpose { get; set; }

        string MFICenterReferenceNo { get; set; }

        string MFIBranchReferenceNo { get; set; }

        string Product { get; set; }
        string CibilRequestBuilderRule { get; set; }
        string EnvironmentId { get; set; }
        string SoapActionMetaDataXMLStringUrl { get; set; }
        string SoapActionDownloadDocumentUrl { get; set; }
    }
}