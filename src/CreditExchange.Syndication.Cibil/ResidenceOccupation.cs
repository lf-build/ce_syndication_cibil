﻿namespace CreditExchange.Syndication.Cibil
{
    public enum ResidenceOccupation
    {
        Owned=01,
        Rented=02
    }
}