﻿
namespace CreditExchange.Syndication.Cibil.CIR
{
    public class TelephoneSegment : ITelephoneSegment
    {
        public TelephoneSegment()
        {

        }
        public TelephoneSegment(Proxy.Response.TelephoneSegment telephone)
        {
            TelephoneNumber = telephone.TelephoneNumber;
            TelephoneType = telephone.TelephoneType;
        }
        public string TelephoneNumber { get; set; }
        
        public string TelephoneType { get; set; }
    }
}
