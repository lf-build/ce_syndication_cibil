﻿
namespace CreditExchange.Syndication.Cibil.CIR
{
    public class Enquiry : IEnquiry
    {
        public Enquiry()
        {

        }
        public Enquiry(Proxy.Response.Enquiry enquiry)
        {
            if(enquiry!=null)
            {
                DateOfEnquiryFields = enquiry.DateOfEnquiryFields;
                EnquiringMemberShortName = enquiry.EnquiringMemberShortName;
                EnquiryPurpose = enquiry.EnquiryPurpose;
                EnquiryAmount = enquiry.EnquiryAmount;
            }
        }
       public string DateOfEnquiryFields { get; set; }
       public string EnquiringMemberShortName { get; set; }
       public string EnquiryPurpose { get; set; }
       public string EnquiryAmount { get; set; }
    }

  
}
