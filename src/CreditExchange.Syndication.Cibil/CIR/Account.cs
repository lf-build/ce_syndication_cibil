﻿
namespace CreditExchange.Syndication.Cibil.CIR
{
    public class Account : IAccount
    {
        public Account()
        {

        }
        public Account(Proxy.Response.Account_NonSummary_Segment_Fields account_NonSummary_Segment_Fields)
        {
            if (account_NonSummary_Segment_Fields != null)
            {

                ReportingMemberShortName = account_NonSummary_Segment_Fields?.ReportingMemberShortName;
                AccountType = account_NonSummary_Segment_Fields?.AccountType;
                OwenershipIndicator = account_NonSummary_Segment_Fields?.OwenershipIndicator;
                DateOpenedOrDisbursed = account_NonSummary_Segment_Fields?.DateOpenedOrDisbursed;
                DateOfLastPayment = account_NonSummary_Segment_Fields?.DateOfLastPayment;
                DateReportedAndCertified = account_NonSummary_Segment_Fields?.DateReportedAndCertified;
                HighCreditOrSanctionedAmount = account_NonSummary_Segment_Fields?.HighCreditOrSanctionedAmount;
                CurrentBalance = account_NonSummary_Segment_Fields?.CurrentBalance;
                PaymentHistory1 = account_NonSummary_Segment_Fields?.PaymentHistory1;
                PaymentHistoryStartDate = account_NonSummary_Segment_Fields?.PaymentHistoryStartDate;
                PaymentHistoryEndDate = account_NonSummary_Segment_Fields?.PaymentHistoryEndDate;
                CreditLimit = account_NonSummary_Segment_Fields?.CreditLimit;
                CashLimit = account_NonSummary_Segment_Fields?.CashLimit;
                RateOfInterest = account_NonSummary_Segment_Fields?.RateOfInterest;
                PaymentFrequency = account_NonSummary_Segment_Fields?.PaymentFrequency;
                ActualPaymentAmount = account_NonSummary_Segment_Fields?.ActualPaymentAmount;
                PaymentHistory2 = account_NonSummary_Segment_Fields?.PaymentHistory2;
                DateClosed = account_NonSummary_Segment_Fields?.DateClosed;
                PaymentHistory1FieldLength = account_NonSummary_Segment_Fields?.PaymentHistory1FieldLength;
                PaymentHistory2FieldLength = account_NonSummary_Segment_Fields?.PaymentHistory2FieldLength;
                EmiAmount = account_NonSummary_Segment_Fields?.EmiAmount;
                RepaymentTenure = account_NonSummary_Segment_Fields?.RepaymentTenure;
                AmountOverdue = account_NonSummary_Segment_Fields?.AmountOverdue;
                SuitFiledOrWilfulDefault = account_NonSummary_Segment_Fields?.SuitFiledOrWilfulDefault;
                WrittenOffAndSettled = account_NonSummary_Segment_Fields?.WrittenOffAndSettled;

            }
        }
        public string ReportingMemberShortName { get; set; }

        public string AccountType { get; set; }

        public string OwenershipIndicator { get; set; }

        public string DateOpenedOrDisbursed { get; set; }

        public string DateOfLastPayment { get; set; }

        public string DateReportedAndCertified { get; set; }

        public string HighCreditOrSanctionedAmount { get; set; }

        public string CurrentBalance { get; set; }

        public string PaymentHistory1 { get; set; }

        public string PaymentHistoryStartDate { get; set; }

        public string PaymentHistoryEndDate { get; set; }

        public string PaymentHistory1FieldLength { get; set; }

        public string PaymentHistory2FieldLength { get; set; }
        public string CreditLimit { get; set; }

        public string CashLimit { get; set; }

        public string RateOfInterest { get; set; }

        public string PaymentFrequency { get; set; }

        public string ActualPaymentAmount { get; set; }

        public string PaymentHistory2 { get; set; }

        public string DateClosed { get; set; }
        public string RepaymentTenure { get; set; }
        public string EmiAmount { get; set; }

        public string SuitFiledOrWilfulDefault { get; set; }

        public string WrittenOffAndSettled { get; set; }
        public string AmountOverdue { get; set; }
    }
}
