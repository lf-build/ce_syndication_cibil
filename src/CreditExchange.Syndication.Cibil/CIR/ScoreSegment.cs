﻿
namespace CreditExchange.Syndication.Cibil.CIR
{
    public class ScoreSegment : IScoreSegment
    {
        public ScoreSegment()
        {

        }

        public ScoreSegment(Proxy.Response.ScoreSegment scoresegment)
        {
            if(scoresegment!=null)
            {
                ScoreName = scoresegment.ScoreName;
                ScoreCardName = scoresegment.ScoreCardName;
                ScoreCardVersion = scoresegment.ScoreCardVersion;
                ScoreDate = scoresegment.ScoreDate;
                Score = scoresegment.Score;
                ReasonCode1 = scoresegment.ReasonCode1;
                ReasonCode2 = scoresegment.ReasonCode2;
            }
        }
        public string ScoreName { get; set; }
        
        public string ScoreCardName { get; set; }
        
        public string ScoreCardVersion { get; set; }
        
        public string ScoreDate { get; set; }
        
        public string Score { get; set; }
        
        public string ReasonCode1 { get; set; }
        
          
        public string ReasonCode2 { get; set; }
        public string ReasonCode3 { get; set; }


        public string ReasonCode4 { get; set; }
        public string ReasonCode5 { get; set; }

             

    }
}
