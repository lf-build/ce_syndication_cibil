﻿namespace CreditExchange.Syndication.Cibil.CIR
{
    public interface IEmploymentSegment
    {
        string AccountType { get; set; }
        string DateReportedCertified { get; set; }
        string Income { get; set; }
         string MonthlyAnnualIndicator { get; set; }
        string NetGrossIndicator { get; set; }
        string OccupationCode { get; set; }
    }
}