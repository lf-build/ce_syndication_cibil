﻿

namespace CreditExchange.Syndication.Cibil.CreditBureau
{
    public class Pan : IPan
    {
        public Pan()
        {

        }
        public Pan(Proxy.Response.PAN pan)
        {
            if(pan!=null)
            {
                PANName = pan.PANName;
                PANNameMatch = pan.PANNameMatch;
                PANNo = pan.PANNo;
                PANNoMatch = pan.PANNoMatch;
            }
        }
        public Pan(Proxy.Response.CPVAttributes cpvAttributes)
        {
            if(cpvAttributes != null)
            {
                PANNameMatch = cpvAttributes?.Match?.IDNSDLNameMatch;
                PANNoMatch = cpvAttributes?.Match?.IDNSDLIdentifierMatch;
                PANName = cpvAttributes?.Pan?.Name;
                PANNo = cpvAttributes?.Pan?.ID;
            }
        }
        public string PANName { get; set; }
    
        public string PANNameMatch { get; set; }
    
        public string PANNo { get; set; }
    
        public string PANNoMatch { get; set; }
    }
}
