﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditExchange.Syndication.Cibil.CreditBureau
{
    public class Name : IName
    {
        public Name()
        {

        }
        public Name(Proxy.Response.Name name)
        {
            FirstName = name.FirstName;
            MiddleName = name.MiddleName;
            LastName = name.LastName;
            NameMatch = name.NameMatch;
        }
        public string FirstName { get; set; }
     
        public string MiddleName { get; set; }
     
        public string LastName { get; set; }
     
        public string NameMatch { get; set; }
    }
}
