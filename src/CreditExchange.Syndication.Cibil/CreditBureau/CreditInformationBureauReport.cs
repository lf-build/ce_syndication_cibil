﻿
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;

namespace CreditExchange.Syndication.Cibil.CreditBureau
{
    public class CreditInformationBureauReport : ICreditInformationBureauReport
    {
        public CreditInformationBureauReport()
        {

        }
        public CreditInformationBureauReport(Proxy.Response.Output response)
        {
            if (response != null)
            {
                if(response?.EKYC!=null)
                Ekyc = new Ekyc(response?.EKYC);
                if (response?.VoterId != null)
                    VoterId = new VoterId(response?.VoterId);
                if(response?.AadhaarIdBA!=null)
                AadhaarIdBA = new AadhaarIdBA(response?.AadhaarIdBA);
                if (response?.DataSource != null)
                    DataSource = new CreditBureau.Datasource(response?.DataSource);
                if (response?.PAN != null)
                    Pan = new Pan(response?.PAN);
                Cibildata = new Cibildata(response?.CIBIL);
            }

        }

        [JsonConverter(typeof(InterfaceConverter<IEkyc, Ekyc>))]
        public IEkyc Ekyc { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IVoterId, VoterId>))]
        public IVoterId VoterId { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IAadhaarIdBA, AadhaarIdBA>))]
        public IAadhaarIdBA AadhaarIdBA { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IDatasource, Datasource>))]
        public IDatasource DataSource { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IPan, Pan>))]
        public IPan Pan { get; set; }

        [JsonConverter(typeof(InterfaceConverter<ICibildata, Cibildata>))]
        public ICibildata Cibildata { get; set; }
    }
}
