﻿
namespace CreditExchange.Syndication.Cibil.CreditBureau
{
    public class VoterId : IVoterId
    {
        public VoterId()
        {

        }
        public VoterId(Proxy.Response.VoterId voterid)
        {
            Id = voterid.Id;
            Source = voterid.Source;
            ErrorCode = voterid.ErrorCode;
            TimeTrail = voterid.TimeTrail;
        }
        public string Id { get; set; }
     
        public string Source { get; set; }
     
        public string ErrorCode { get; set; }
     
        public string TimeTrail { get; set; }
    }
}
