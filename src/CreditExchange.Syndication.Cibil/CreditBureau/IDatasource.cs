﻿using System.Collections.Generic;

namespace CreditExchange.Syndication.Cibil.CreditBureau
{
    public interface IDatasource
    {
        List<IAddress> Address { get; set; }
        string DateofBirth { get; set; }
        string DateofBirthMatch { get; set; }
        string DrivingLicense { get; set; }
        string DrivingLicenseMatch { get; set; }
        string FirstName { get; set; }
        string Gender { get; set; }
        string GenderMatch { get; set; }
        string LastName { get; set; }
        string MiddleName { get; set; }
        string NameMatch { get; set; }
        string PAN { get; set; }
        string PANMatch { get; set; }
        string Passport { get; set; }
        string PassportMatch { get; set; }
        string RationCard { get; set; }
        string RationCardMatch { get; set; }
        string Source { get; set; }
        string Telephone1 { get; set; }
        string Telephone1Match { get; set; }
        string Telephone2 { get; set; }
        string Telephone2Match { get; set; }
        string Telephone3 { get; set; }
        string Telephone3Match { get; set; }
        string Telephone4 { get; set; }
        string Telephone4Match { get; set; }
        string UID { get; set; }
        string UIDMatch { get; set; }
        string VoterId { get; set; }
        string VoterIdMatch { get; set; }
    }
}