﻿namespace CreditExchange.Syndication.Cibil.CreditBureau
{
    public interface IVoterId
    {
        string ErrorCode { get; set; }
        string Id { get; set; }
        string Source { get; set; }
        string TimeTrail { get; set; }
    }
}