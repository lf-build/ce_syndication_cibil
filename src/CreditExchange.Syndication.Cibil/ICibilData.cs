﻿using LendFoundry.Foundation.Persistence;
using System;

namespace CreditExchange.Syndication.Cibil
{
    public interface ICibilData :IAggregate
    {
        string EntityId { get; set; }
        string EntityType { get; set; }
        string RequestXML { get; set; }
        string ResponseXML { get; set; }
        DateTimeOffset ReportDate { get; set; }

        string BureauResponseRaw { get; set; }
        string Status { get; set; }
    }
}