﻿
using System;

namespace CreditExchange.Syndication.Cibil.Request
{
    public class ReportRequest  :IReportRequest
    {
        public  string ApplicantFirstName { get; set; }
        public  string ApplicantMiddleName { get; set; }
        public  string ApplicantLastName { get; set; }
        public string Gender { get; set; }
        public DateTimeOffset DateOfBirth { get; set; }
        public string EmailId { get; set; }
        public string PanNo { get; set; }
        public  string DrivingLicenseNumber { get; set; }
        public  string VoterId { get; set; }
        public  string AdharNumber { get; set; }
        public  string PassportNumber { get; set; }
        public  string RationCardNo { get; set; }
        public  string Address1Line1 { get; set; }
        public string Address1Line2 { get; set; }
        public string Address1Line3 { get; set; }
        public string Address1City { get; set; }
        public string Address1State { get; set; } 
        public  string Address1Pincode { get; set; }
        public string Address1AddressCategory { get; set; }
        public string Address1ResidentOccupation { get; set; }
             
        public string Address2Line1 { get; set; }
        public string Address2Line2 { get; set; }
        public string Address2Line3 { get; set; }
        public string Address2City { get; set; }
        public string Address2State { get; set; }
        public string Address2Pincode { get; set; }
        public string Address2AddressCategory { get; set; }
        public string Address2ResidentOccupation { get; set; }
        public string MobileNo { get; set; }
        public string LandLineNo { get; set; }
        public  string OfficeLandLineNo { get; set; }    
        public string LoanAmount { get; set; }
        public string GSTStateCode { get; set; }
        public string CompanyName { get; set; }
    }
}
