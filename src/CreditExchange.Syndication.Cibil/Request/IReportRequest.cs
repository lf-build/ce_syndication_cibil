﻿using System;

namespace CreditExchange.Syndication.Cibil.Request
{
    public interface IReportRequest
    {
        string Address1AddressCategory { get; set; }
        string Address1City { get; set; }
        string Address1Line1 { get; set; }
        string Address1Line2 { get; set; }
        string Address1Line3 { get; set; }
        string Address1Pincode { get; set; }
        string Address1ResidentOccupation { get; set; }
        string Address1State { get; set; }
        string Address2AddressCategory { get; set; }
        string Address2City { get; set; }
        string Address2Line1 { get; set; }
        string Address2Line2 { get; set; }
        string Address2Line3 { get; set; }
        string Address2Pincode { get; set; }
        string Address2ResidentOccupation { get; set; }
        string Address2State { get; set; }
        string AdharNumber { get; set; }
        string ApplicantFirstName { get; set; }
        string ApplicantLastName { get; set; }
        string ApplicantMiddleName { get; set; }
        DateTimeOffset DateOfBirth { get; set; }
        string DrivingLicenseNumber { get; set; }
        string EmailId { get; set; }
        string Gender { get; set; }
        string LandLineNo { get; set; }
        string MobileNo { get; set; }
        string OfficeLandLineNo { get; set; }
        string PanNo { get; set; }
        string PassportNumber { get; set; }
        string RationCardNo { get; set; }
        string VoterId { get; set; }
        string LoanAmount { get; set; }
        string CompanyName { get; set; }
        string GSTStateCode { get; set; }
    }
}