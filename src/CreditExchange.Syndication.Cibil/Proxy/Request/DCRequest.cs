﻿
using System.Collections.Generic;
using System.Xml.Serialization;

namespace CreditExchange.Syndication.Cibil.Proxy.Request
{

    [XmlRoot(ElementName = "Authentication", Namespace = "http://transunion.com/dc/extsvc")]
    public partial class Authentication
    {
        [XmlElement(ElementName = "UserId", Namespace = "http://transunion.com/dc/extsvc")]
        public string UserId { get; set; }
        [XmlElement(ElementName = "Password", Namespace = "http://transunion.com/dc/extsvc")]
        public string Password { get; set; }
        [XmlAttribute(AttributeName = "type")]
        public string Type { get; set; }
    }

    [XmlRoot(ElementName = "RequestInfo", Namespace = "http://transunion.com/dc/extsvc")]
    public partial class RequestInfo
    {
        [XmlElement(ElementName = "SolutionSetId", Namespace = "http://transunion.com/dc/extsvc")]
        public string SolutionSetId { get; set; }
        [XmlElement(ElementName = "SolutionSetVersion", Namespace = "http://transunion.com/dc/extsvc")]
        public string SolutionSetVersion { get; set; }
        [XmlElement(ElementName = "ExecutionMode", Namespace = "http://transunion.com/dc/extsvc")]
        public string ExecutionMode { get; set; }
        [XmlElement(ElementName = "EnvironmentId", Namespace = "http://transunion.com/dc/extsvc")]
        public string EnvironmentId { get; set; }
    }

    [XmlRoot(ElementName = "Field", Namespace = "http://transunion.com/dc/extsvc")]
    public partial class Field
    {
        [XmlAttribute(AttributeName = "key")]
        public string Key { get; set; }
        [XmlText]
        public string Text { get; set; }
    }
    [XmlRoot(ElementName = "Fields", Namespace = "http://transunion.com/dc/extsvc")]
    public class Fields
    {
        [XmlElement(ElementName = "Field", Namespace = "http://transunion.com/dc/extsvc")]
        public List<Field> Field { get; set; }
    }
    [XmlRoot(ElementName = "Address")]
    public partial class Address
    {
        [XmlElement(ElementName = "AddressLine1")]
        public string AddressLine1 { get; set; }
        [XmlElement(ElementName = "AddressLine2")]
        public string AddressLine2 { get; set; }
        [XmlElement(ElementName = "AddressLine3")]
        public string AddressLine3 { get; set; }
        [XmlElement(ElementName="AddressLine4")]
		public string AddressLine4 { get; set; }
		[XmlElement(ElementName="AddressLine5")]
		public string AddressLine5 { get; set; }
        [XmlElement(ElementName = "AddressType")]
        public string AddressType { get; set; }
        [XmlElement(ElementName = "City")]
        public string City { get; set; }
        [XmlElement(ElementName = "PinCode")]
        public string PinCode { get; set; }
        [XmlElement(ElementName = "ResidenceType")]
        public string ResidenceType { get; set; }
        [XmlElement(ElementName = "StateCode")]
        public string StateCode { get; set; }
    }
    [XmlRoot(ElementName = "Telephone")]
    public partial class Telephone
    {
        [XmlElement(ElementName = "TelephoneType")]
        public string TelephoneType { get; set; }
        [XmlElement(ElementName = "TelephoneNumber")]
        public string TelephoneNumber { get; set; }
        [XmlElement(ElementName = "TelephoneExtension")]
        public string TelephoneExtension { get; set; }
    }
    [XmlRoot(ElementName = "Identifier")]
    public partial class Identifier
    {
        [XmlElement(ElementName = "IdType")]
        public string IdType { get; set; }
        [XmlElement(ElementName = "IdNumber")]
        public string IdNumber { get; set; }
    }

    [XmlRoot(ElementName = "Identifiers")]
    public partial class Identifiers
    {
        [XmlElement(ElementName = "Identifier")]
        public List<Identifier> Identifier { get; set; }
    }
    [XmlRoot(ElementName = "Telephones")]
    public partial class Telephones
    {
        [XmlElement(ElementName = "Telephone")]
        public List<Telephone> Telephone { get; set; }
    }
    [XmlRoot(ElementName = "Addresses")]
    public partial class Addresses
    {
        [XmlElement(ElementName = "Address")]
        public List<Address> Address { get; set; }
    }
    [XmlRoot(ElementName = "Applicants")]
    public partial class Applicants
    {
        [XmlElement(ElementName = "Applicant")]
        public List<Applicant> Applicant { get; set; }
    }
    [XmlRoot(ElementName = "Applicant")]
    public partial class Applicant
    {
        [XmlElement(ElementName = "ApplicantType")]
        public string ApplicantType { get; set; }
        [XmlElement(ElementName = "ApplicantFirstName")]
        public string ApplicantFirstName { get; set; }
        [XmlElement(ElementName = "ApplicantLastName")]
        public string ApplicantLastName { get; set; }
        [XmlElement(ElementName = "ApplicantMiddleName")]
        public string ApplicantMiddleName { get; set; }
        [XmlElement(ElementName = "DateOfBirth")]
        public string DateOfBirth { get; set; }
        [XmlElement(ElementName = "Gender")]
        public string Gender { get; set; }
        [XmlElement(ElementName = "EmailAddress")]
        public string EmailAddress { get; set; }
        [XmlElement(ElementName="CompanyName")]
		public string CompanyName { get; set; }
        [XmlElement(ElementName = "Identifiers")]
        public Identifiers Identifiers { get; set; }
        [XmlElement(ElementName = "Telephones")]
        public Telephones Telephones { get; set; }
        [XmlElement(ElementName = "Addresses")]
        public Addresses Addresses { get; set; }
        public string NomineeRelation { get; set; }
		[XmlElement(ElementName="NomineeName")]
		public string NomineeName { get; set; }
		[XmlElement(ElementName="MemberRelationType4")]
		public string MemberRelationType4 { get; set; }
		[XmlElement(ElementName="MemberRelationName4")]
		public string MemberRelationName4 { get; set; }
		[XmlElement(ElementName="MemberRelationType3")]
		public string MemberRelationType3 { get; set; }
		[XmlElement(ElementName="MemberRelationName3")]
		public string MemberRelationName3 { get; set; }
		[XmlElement(ElementName="MemberRelationType2")]
		public string MemberRelationType2 { get; set; }
		[XmlElement(ElementName="MemberRelationName2")]
		public string MemberRelationName2 { get; set; }
		[XmlElement(ElementName="MemberRelationType1")]
		public string MemberRelationType1 { get; set; }
		[XmlElement(ElementName="MemberRelationName1")]
		public string MemberRelationName1 { get; set; }
		[XmlElement(ElementName="KeyPersonRelation")]
		public string KeyPersonRelation { get; set; }
		[XmlElement(ElementName="KeyPersonName")]
		public string KeyPersonName { get; set; }
		[XmlElement(ElementName="MemberOtherId3")]
		public string MemberOtherId3 { get; set; }
		[XmlElement(ElementName="MemberOtherId3Type")]
		public string MemberOtherId3Type { get; set; }
		[XmlElement(ElementName="MemberOtherId2")]
		public string MemberOtherId2 { get; set; }
		[XmlElement(ElementName="MemberOtherId2Type")]
		public string MemberOtherId2Type { get; set; }
		[XmlElement(ElementName="MemberOtherId1")]
		public string MemberOtherId1 { get; set; }
		[XmlElement(ElementName="MemberOtherId1Type")]
		public string MemberOtherId1Type { get; set; }
        [XmlElement(ElementName="Accounts")]
		public Accounts Accounts { get; set; }
    }

    [XmlRoot(ElementName = "ApplicationData")]
    public partial class ApplicationData
    {
       [XmlElement(ElementName="Purpose")]
		public string Purpose { get; set; }
		[XmlElement(ElementName="Amount")]
		public string Amount { get; set; }
		[XmlElement(ElementName="ScoreType")]
		public string ScoreType { get; set; }
		[XmlElement(ElementName="GSTStateCode")]
		public string GSTStateCode { get; set; }
		[XmlElement(ElementName="MemberCode")]
		public string MemberCode { get; set; }
		[XmlElement(ElementName="Password")]
		public string Password { get; set; }
		[XmlElement(ElementName="CibilBureauFlag")]
		public string CibilBureauFlag { get; set; }
		[XmlElement(ElementName="DSTuNtcFlag")]
		public string DSTuNtcFlag { get; set; }
		[XmlElement(ElementName="IDVerificationFlag")]
		public string IDVerificationFlag { get; set; }
		[XmlElement(ElementName="MFIBureauFlag")]
		public string MFIBureauFlag { get; set; }
		[XmlElement(ElementName="NTCProductType")]
		public string NTCProductType { get; set; }
		[XmlElement(ElementName="ConsumerConsentForUIDAIAuthentication")]
		public string ConsumerConsentForUIDAIAuthentication { get; set; }
		[XmlElement(ElementName="MFIEnquiryAmount")]
		public string MFIEnquiryAmount { get; set; }
		[XmlElement(ElementName="MFILoanPurpose")]
		public string MFILoanPurpose { get; set; }
		[XmlElement(ElementName="MFICenterReferenceNo")]
		public string MFICenterReferenceNo { get; set; }
		[XmlElement(ElementName="MFIBranchReferenceNo")]
		public string MFIBranchReferenceNo { get; set; }
		[XmlElement(ElementName="FormattedReport")]
		public string FormattedReport { get; set; }
    }

    [XmlRoot(ElementName = "DCRequest", Namespace = "http://transunion.com/dc/extsvc")]
    public partial class DCRequest
    {
        [XmlElement(ElementName = "Authentication", Namespace = "http://transunion.com/dc/extsvc")]
        public Authentication Authentication { get; set; }
        [XmlElement(ElementName = "RequestInfo", Namespace = "http://transunion.com/dc/extsvc")]
        public RequestInfo RequestInfo { get; set; }
        [XmlElement(ElementName = "UserData", Namespace = "http://transunion.com/dc/extsvc")]
        public string UserData { get; set; }
        [XmlElement(ElementName = "Fields", Namespace = "http://transunion.com/dc/extsvc")]
        public Fields Fields { get; set; }
        [XmlAttribute(AttributeName = "xmlns")]
        public string Xmlns { get; set; }
    }

    [XmlRoot(ElementName="Accounts")]
	public class Accounts {
		[XmlElement(ElementName="Account")]
		public Account Account { get; set; }
	}
	[XmlRoot(ElementName="Account")]
	public class Account {
		[XmlElement(ElementName="AccountNumber")]
		public string AccountNumber { get; set; }
	}
}
