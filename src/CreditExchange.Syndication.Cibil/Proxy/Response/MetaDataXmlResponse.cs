using System;
using System.Collections.Generic;
using System.Xml.Serialization;
namespace CreditExchange.Syndication.Cibil.Proxy.Response
{
    [XmlRoot (ElementName = "Authentication")]
    public class MetaDataAuthentication
    {
        [XmlElement (ElementName = "Status")]
        public string Status { get; set; }

        [XmlElement (ElementName = "Token")]
        public string Token { get; set; }
    }

    [XmlRoot (ElementName = "DocumentMetaData")]
    public class DocumentMetaData
    {
        [XmlElement (ElementName = "ApplicationId")]
        public string ApplicationId { get; set; }

        [XmlElement (ElementName = "CompressionMethod")]
        public string CompressionMethod { get; set; }

        [XmlElement (ElementName = "CreatedDate")]
        public string CreatedDate { get; set; }

        [XmlElement (ElementName = "CreatedUser")]
        public string CreatedUser { get; set; }

        [XmlElement (ElementName = "Description")]
        public string Description { get; set; }

        [XmlElement (ElementName = "DirectoryId")]
        public string DirectoryId { get; set; }

        [XmlElement (ElementName = "DocumentId")]
        public string DocumentId { get; set; }

        [XmlElement (ElementName = "EncodingType")]
        public string EncodingType { get; set; }

        [XmlElement (ElementName = "FileSpaceType")]
        public string FileSpaceType { get; set; }

        [XmlElement (ElementName = "GUIDFileName")]
        public string GUIDFileName { get; set; }

        [XmlElement (ElementName = "IsDeleted")]
        public string IsDeleted { get; set; }

        [XmlElement (ElementName = "ModifyDate")]
        public string ModifyDate { get; set; }

        [XmlElement (ElementName = "ModifyUser")]
        public string ModifyUser { get; set; }

        [XmlElement (ElementName = "Note")]
        public string Note { get; set; }

        [XmlElement (ElementName = "OriginalFileName")]
        public string OriginalFileName { get; set; }

        [XmlElement (ElementName = "OriginalFileSize")]
        public string OriginalFileSize { get; set; }

        [XmlElement (ElementName = "Size")]
        public string Size { get; set; }
    }

    [XmlRoot (ElementName = "DocumentDetails")]
    public class DocumentDetails
    {
        [XmlElement (ElementName = "DocumentMetaData")]
        public List<DocumentMetaData> DocumentMetaData { get; set; }
    }

    [XmlRoot (ElementName = "ResponseInfo")]
    public class MetaDataResponseInfo
    {
        [XmlElement (ElementName = "DocumentDetails")]
        public DocumentDetails DocumentDetails { get; set; }
    }

    [XmlRoot (ElementName = "DCResponse")]
    public class MetaDataXmlResponse
    {
        [XmlElement (ElementName = "Status")]
        public string Status { get; set; }

        [XmlElement (ElementName = "Authentication")]
        public Authentication Authentication { get; set; }

        [XmlElement (ElementName = "ResponseInfo")]
        public ResponseInfo ResponseInfo { get; set; }
    }

}