﻿using System.Xml.Serialization;

namespace CreditExchange.Syndication.Cibil.Proxy.Response
{
    [XmlRoot(ElementName = "ConnectivityErrors")]
    public class ConnectivityErrors
    {
        [XmlAttribute(AttributeName = "ErrorCode")]
        public string ErrorCode { get; set; }
        [XmlAttribute(AttributeName = "Desc")]
        public string Desc { get; set; }
    }

    [XmlRoot(ElementName = "Response")]
    public class CustomErrorResponse
    {
        [XmlElement(ElementName = "Status")]
        public string Status { get; set; }
        [XmlElement(ElementName = "ErrorCode")]
        public string ErrorCode { get; set; }
        [XmlElement(ElementName = "ErrorDescription")]
        public string ErrorDescription { get; set; }
        [XmlElement(ElementName = "Segment")]
        public string Segment { get; set; }
    }

    [XmlRoot(ElementName = "ErrorSegment")]
    public class ErrorSegment
    {
        [XmlElement(ElementName = "ConnectivityErrors")]
        public ConnectivityErrors ConnectivityErrors { get; set; }
        [XmlElement(ElementName = "Response")]
        public CustomErrorResponse Response { get; set; }
    }
}
