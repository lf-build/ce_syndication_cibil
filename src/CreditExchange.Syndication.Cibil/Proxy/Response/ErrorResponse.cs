﻿using System.Xml.Serialization;

namespace CreditExchange.Syndication.Cibil.Proxy.Response
{
 
        [XmlRoot(ElementName= "Response")]
	    public class ErrorResponse
        {
            [XmlElement(ElementName = "Status")]
            public string Status { get; set; }
            [XmlElement(ElementName = "ErrorCode")]
            public string ErrorCode { get; set; }
            [XmlElement(ElementName = "ErrorDescription")]
            public string ErrorDescription { get; set; }
            [XmlElement(ElementName = "Segment")]
            public string Segment { get; set; }
        }
    
}
