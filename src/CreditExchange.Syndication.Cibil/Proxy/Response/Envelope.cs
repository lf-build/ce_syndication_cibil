using System;
using System.Collections.Generic;
using System.Xml.Serialization;
namespace CreditExchange.Syndication.Cibil.Proxy.Response
{
    [XmlRoot (ElementName = "DownloadDocumentResult", Namespace = "http://tempuri.org/")]
    public class DownloadDocumentResult
    {
        [XmlElement (ElementName = "FileContent", Namespace = "http://schemas.datacontract.org/2004/07/")]
        public string FileContent { get; set; }

        [XmlElement (ElementName = "Response", Namespace = "http://schemas.datacontract.org/2004/07/")]
        public string Response { get; set; }

        [XmlAttribute (AttributeName = "a", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string A { get; set; }

        [XmlAttribute (AttributeName = "i", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string I { get; set; }
    }

    [XmlRoot (ElementName = "DownloadDocumentResponse", Namespace = "http://tempuri.org/")]
    public class DownloadDocumentResponse
    {
        [XmlElement (ElementName = "DownloadDocumentResult", Namespace = "http://tempuri.org/")]
        public DownloadDocumentResult DownloadDocumentResult { get; set; }

        [XmlAttribute (AttributeName = "xmlns")]
        public string Xmlns { get; set; }
    }

    [XmlRoot (ElementName = "Body", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    public class Body
    {
        [XmlElement (ElementName = "DownloadDocumentResponse", Namespace = "http://tempuri.org/")]
        public DownloadDocumentResponse DownloadDocumentResponse { get; set; }
    }

    [XmlRoot (ElementName = "Envelope", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    public class Envelope
    {
        [XmlElement (ElementName = "Body", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
        public Body Body { get; set; }

        [XmlAttribute (AttributeName = "s", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string S { get; set; }
    }

}