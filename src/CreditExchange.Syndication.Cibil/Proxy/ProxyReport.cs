﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using CreditExchange.Syndication.Cibil.Proxy.Request;
using CreditExchange.Syndication.Cibil.Proxy.Response;
#if DOTNET2
using System.Web;
#else
using RestSharp.Contrib;
#endif
using System.Linq;
using System.Net;
using System.Security;
using System.Text.RegularExpressions;
using System.Xml;
using LendFoundry.Foundation.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;

namespace CreditExchange.Syndication.Cibil.Proxy
{
    public class ProxyReport : IProxyReport
    {
        public ProxyReport (ICibilConfiguration cibilConfiguration, ILogger logger)
        {
            if (cibilConfiguration == null)
                throw new ArgumentNullException (nameof (cibilConfiguration));
            if (string.IsNullOrWhiteSpace (cibilConfiguration.Url))
                throw new ArgumentException ("Url is required", nameof (cibilConfiguration.Url));
            if (string.IsNullOrWhiteSpace (cibilConfiguration.UserId))
                throw new ArgumentException ("UserId is required", nameof (cibilConfiguration.UserId));
            if (string.IsNullOrWhiteSpace (cibilConfiguration.Password))
                throw new ArgumentException ("Password is required", nameof (cibilConfiguration.Password));
            if (string.IsNullOrWhiteSpace (cibilConfiguration.Authentication))
                throw new ArgumentException ("Authentication is required", nameof (cibilConfiguration.Authentication));
            if (string.IsNullOrWhiteSpace (cibilConfiguration.EnvironmentType))
                throw new ArgumentException ("EnvironmentType is required", nameof (cibilConfiguration.EnvironmentType));
            if (string.IsNullOrWhiteSpace (cibilConfiguration.ExecutionMode))
                throw new ArgumentException ("ExecutionMode is required", nameof (cibilConfiguration.ExecutionMode));
            if (string.IsNullOrWhiteSpace (cibilConfiguration.InpEnquiryPurpose))
                throw new ArgumentException ("InpEnquiryPurpose is required", nameof (cibilConfiguration.InpEnquiryPurpose));
            if (string.IsNullOrWhiteSpace (cibilConfiguration.InpScoreType))
                throw new ArgumentException ("InpScoreType is required", nameof (cibilConfiguration.InpScoreType));
            if (string.IsNullOrWhiteSpace (cibilConfiguration.IsCirReq))
                throw new ArgumentException ("IsCirReq is required", nameof (cibilConfiguration.IsCirReq));
            if (string.IsNullOrWhiteSpace (cibilConfiguration.IsEverifyReq))
                throw new ArgumentException ("IsEverifyReq is required", nameof (cibilConfiguration.IsEverifyReq));
            if (string.IsNullOrWhiteSpace (cibilConfiguration.SolutionSetId))
                throw new ArgumentException ("SolutionSetId is required", nameof (cibilConfiguration.SolutionSetId));
            if (string.IsNullOrWhiteSpace (cibilConfiguration.CibilRequestBuilderRule))
                throw new ArgumentException ("CibilRequestBuilderRule is required", nameof (cibilConfiguration.SolutionSetVersion));
            CibilConfiguration = cibilConfiguration;
            Logger = logger;

        }
        private ILogger Logger { get; }
        private ICibilConfiguration CibilConfiguration { get; }
        public async Task<XMLResponse> GetCreditReport (DCRequest dcRequest)
        {
            if (dcRequest == null)
                throw new ArgumentNullException (nameof (dcRequest));

            var client = new RestClient (CibilConfiguration.Url);
            var request = new RestRequest (Method.POST);
            request.AddHeader ("SOAPAction", "http://tempuri.org/IExternalSolutionExecution/ExecuteXMLString");
            request.AddHeader ("Content-Type", "text/xml");
            request.AddHeader ("Accept", "text/plain");
            var dcRequestXml = XmlSerialization.Serialize (dcRequest, "http://transunion.com/dc/extsvc", "", false);
            Logger.Info ($"[Cibil] GetCreditReport method xmlrequest #{dcRequestXml} ");
            var cibilreportxmlstring = dcRequestXml;
            cibilreportxmlstring = cibilreportxmlstring.Replace ("&amp;", "&").Replace ("&quot;", "\"");
            cibilreportxmlstring = "<![CDATA[" + cibilreportxmlstring + "]]>";
            var evelopeRequestXml = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\"><soapenv:Header /><soapenv:Body><tem:ExecuteXMLString><tem:request>" +
                cibilreportxmlstring + "</tem:request></tem:ExecuteXMLString></soapenv:Body></soapenv:Envelope>";
            request.AddParameter ("text/xml", evelopeRequestXml, ParameterType.RequestBody);
            try
            {

                TaskCompletionSource<IRestResponse> taskCompletion = new TaskCompletionSource<IRestResponse> ();

                RestRequestAsyncHandle handle = client.ExecuteAsync (request, restResponse => taskCompletion.SetResult (restResponse));
                IRestResponse response = (IRestResponse) (await taskCompletion.Task);
                var deCodeResponse = WebUtility.HtmlDecode (response.Content.Replace ("<s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\"><s:Body><ExecuteXMLStringResponse xmlns=\"http://tempuri.org/\"><ExecuteXMLStringResult>", "").Replace ("</ExecuteXMLStringResult></ExecuteXMLStringResponse></s:Body></s:Envelope>", ""));
                var dcResponse = XmlSerialization.Deserialize<DCResponse> (deCodeResponse);
                return new XMLResponse (dcResponse, dcRequestXml, deCodeResponse);
            }
            catch (WebException e)
            {
                using (WebResponse response = e.Response)
                {
                    HttpWebResponse httpResponse = (HttpWebResponse) response;
                    Logger.Error ($"[Cibil] Error occured when GetCreditReport method was called. Error:", e);

                    using (Stream data = response.GetResponseStream ())
                    {
                        string text = new StreamReader (data).ReadToEnd ();
                        return new XMLResponse (null, dcRequestXml, text);
                    }
                }
            }
            catch (Exception e)
            {
                return null;
            }
        }
        public async Task<string> GetHTMLReport (string cibilReport, ICibilConfiguration cibilConfiguration)
        {
            if (cibilReport == null)
                throw new ArgumentNullException (nameof (cibilReport));
            if (cibilConfiguration == null)
                throw new ArgumentNullException (nameof (cibilConfiguration));
            Logger.Info ($"[Cibil] GetHTMLReport method invoked for data { (cibilReport != null ? JsonConvert.SerializeObject(cibilReport) : null)}");
            var creditReportDocumentId = "";
            var dcResponse = XmlSerialization.Deserialize<DCResponse> (cibilReport);
            var creditReportApplicationId = dcResponse?.ResponseInfo?.ApplicationId;
            if (creditReportApplicationId == null || creditReportApplicationId.Length <= 0)
                throw new ArgumentNullException (nameof (creditReportApplicationId));

            Logger.Info ($"[Cibil] GetHTMLReportMetaDataXMLStringUrl method invoked for ApplicationId {creditReportApplicationId}");
            // tls1.2 
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            var metaDataxmlString = GetXmlString (creditReportApplicationId, "", cibilConfiguration);

            var HTMLReportMetaDataXMLString = await ExecuteXmlHtmlReportRequestAsync (metaDataxmlString, cibilConfiguration.Url, cibilConfiguration.SoapActionMetaDataXMLStringUrl);
            if (HTMLReportMetaDataXMLString == null || HTMLReportMetaDataXMLString.Length <= 0)
            {
                throw new ArgumentNullException (nameof (HTMLReportMetaDataXMLString));
            }
            var xmlstring = WebUtility.HtmlDecode (HTMLReportMetaDataXMLString
                .Replace ("<s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\">", "")
                .Replace ("<s:Body>", "")
                .Replace ("<RetrieveDocumentMetaDataXMLStringResponse xmlns=\"http://tempuri.org/\">", "")
                .Replace ("<RetrieveDocumentMetaDataXMLStringResult>", "")
                .Replace ("<?xml version=\"1.0\"?>", "")
                .Replace ("</RetrieveDocumentMetaDataXMLStringResult>", "")
                .Replace ("</RetrieveDocumentMetaDataXMLStringResponse>", "")
                .Replace ("</s:Body>", "")
                .Replace ("</s:Envelope>", "")
            );
            JObject data = new JObject ();
            try
            {
                //xmlstring = RemoveTroublesomeCharacters (xmlstring);
                // xmlstring = Regex.Replace (xmlstring, @"[^\u0000-\u007F]", String.Empty);
                // var metaDataXmlResponse = XmlSerialization.Deserialize<MetaDataXmlResponse> (xmlstring);
                XmlDocument xml = new XmlDocument ();
                xml.LoadXml (xmlstring);
                // to get one node documentId from xml response.
                string json = JsonConvert.SerializeXmlNode (xml);
                data = (JObject) JsonConvert.DeserializeObject (json);

                int i = 0;
                // getting multiple document id html,pdf.
                //do untill will get first .html report document Id.
                do
                {
                    string reportFileName = (data["DCResponse"]["ResponseInfo"]["DocumentDetails"]["DocumentMetaData"][i]["OriginalFileName"]).Value<string> ();
                    if (reportFileName.Contains (".html"))
                    {
                        creditReportDocumentId = (data["DCResponse"]["ResponseInfo"]["DocumentDetails"]["DocumentMetaData"][i]["DocumentId"]).Value<string> ();
                        break;
                    }
                    i++;
                } while ((data["DCResponse"]["ResponseInfo"]["DocumentDetails"]["DocumentMetaData"][i]["OriginalFileName"]).Value<string> ().Length > 0);
            }
            catch (System.Exception ex)
            {
                //only one documentMetadata tag, No array
                creditReportDocumentId = (data["DCResponse"]["ResponseInfo"]["DocumentDetails"]["DocumentMetaData"]["DocumentId"]).Value<string> ();

            }
            Logger.Info ($"[Cibil] GetHTMLReportDownloadDocumentUrl method invoked for DocumentId {creditReportDocumentId}");
            var reportXmlDownloadString = GetXmlString (creditReportApplicationId, creditReportDocumentId, cibilConfiguration);

            var HTMLReportDownloadString = await ExecuteXmlHtmlReportRequestAsync (reportXmlDownloadString, cibilConfiguration.Url, cibilConfiguration.SoapActionDownloadDocumentUrl);
            if (HTMLReportDownloadString == null || HTMLReportDownloadString.Length <= 0)
            {
                throw new ArgumentNullException (nameof (HTMLReportDownloadString));
            }
            var htmlXmlReportstring = WebUtility.HtmlDecode (HTMLReportDownloadString
                .Replace ("<s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\">", "")
                .Replace ("<s:Body>", "")
                .Replace ("<DownloadDocumentResponse xmlns=\"http: //tempuri.org/\">", "")
                .Replace ("<RetrieveDocumentMetaDataXMLStringResult>", "")
                .Replace ("</DownloadDocumentResponse>", "")
                .Replace ("</s:Body>", "")
                .Replace ("</s:Envelope>", "")
            );

            CreditExchange.Syndication.Cibil.Proxy.Response.Envelope htmlReportResponse = XmlSerialization.Deserialize<CreditExchange.Syndication.Cibil.Proxy.Response.Envelope> (HTMLReportDownloadString);
            if (htmlReportResponse?.Body?.DownloadDocumentResponse?.DownloadDocumentResult?.FileContent.Length > 0)
            {
                return Base64Decode (htmlReportResponse?.Body?.DownloadDocumentResponse?.DownloadDocumentResult?.FileContent);
            }
            return "no file content found";
        }

        private static string DecodeFromUtf8 (string utf8String)
        {
            // copy the string as UTF-8 bytes.
            byte[] utf8Bytes = new byte[utf8String.Length];
            for (int i = 0; i < utf8String.Length; ++i)
            {
                //Debug.Assert( 0 <= utf8String[i] && utf8String[i] <= 255, "the char must be in byte's range");
                utf8Bytes[i] = (byte) utf8String[i];

            }

            return Encoding.UTF8.GetString (utf8Bytes, 0, utf8Bytes.Length);
        }
        public static string Base64Decode (string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String (base64EncodedData);
            return System.Text.Encoding.UTF8.GetString (base64EncodedBytes);
        }
        private async Task<string> ExecuteXmlHtmlReportRequestAsync (string metaDataxmlString, string url, string SoapAction)
        {
            var client = new RestClient (url);
            var request = new RestRequest (Method.POST);
            request.AddHeader ("SOAPAction", SoapAction);
            request.AddHeader ("Content-Type", "text/xml");
            request.AddHeader ("Accept", "text/plain");
            request.AddParameter ("text/xml", metaDataxmlString, ParameterType.RequestBody);
            try
            {
                TaskCompletionSource<IRestResponse> taskCompletion = new TaskCompletionSource<IRestResponse> ();
                RestRequestAsyncHandle handle = client.ExecuteAsync (request, restResponse => taskCompletion.SetResult (restResponse));
                IRestResponse response = (IRestResponse) (await taskCompletion.Task);
                return response.Content;
            }
            catch (Exception e)
            {
                throw new Exception (e.Message);
            }
        }
        private string GetXmlString (string applicationId, string documentId, ICibilConfiguration cibilConfiguration)
        {
            var xmlStart = "<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:tem='http://tempuri.org/'><soapenv:Header/><soapenv:Body>";
            var xmlEnd = "</soapenv:Body></soapenv:Envelope>";
            var password = cibilConfiguration.Password;
            var userId = cibilConfiguration.UserId;

            var xmlCenterMetaDataXMLString = $"<tem:RetrieveDocumentMetaDataXMLString><tem:request><![CDATA[<DCRequest xmlns='http://transunion.com/dc/extsvc'><Authentication type='Token'><UserId>{userId}</UserId><Password>{password}</Password></Authentication><RetrieveDocumentMetaData><ApplicationId>{applicationId}</ApplicationId></RetrieveDocumentMetaData></DCRequest>]]></tem:request></tem:RetrieveDocumentMetaDataXMLString>";
            var xmlCenterDownloadDocument = $"<tem:DownloadDocument><tem:request><![CDATA[<DCRequest xmlns=\"http://transunion.com/dc/extsvc\"><Authentication type=\"Token\"><UserId>{userId}</UserId><Password>{password}</Password></Authentication><DownloadDocument><ApplicationId>{applicationId}</ApplicationId><DocumentId>{documentId}</DocumentId></DownloadDocument></DCRequest>]]></tem:request></tem:DownloadDocument>";

            if (documentId.Length > 0)
            {
                return xmlStart + xmlCenterDownloadDocument + xmlEnd;
            }
            else
            {
                return xmlStart + xmlCenterMetaDataXMLString + xmlEnd;
            }
        }
    }
}