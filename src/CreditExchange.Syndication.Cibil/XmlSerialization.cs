﻿using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace CreditExchange.Syndication.Cibil
{
    public static class XmlSerialization
    {
        public static string Serialize<T>(T @object,string rootnamespace,string prefix,bool omitxmlDeclaration)
        {
            string xmlContent;
            var xmlSerializer = new XmlSerializer(typeof(T));
            using (var sww = new ExtentedStringWriter(Encoding.UTF8))
            using (var writer = XmlWriter.Create(sww, new XmlWriterSettings() { NamespaceHandling = NamespaceHandling.OmitDuplicates, OmitXmlDeclaration = omitxmlDeclaration }))
            {
                var namespaces = new XmlSerializerNamespaces();
                //namespaces.Add(string.Empty, string.Empty);
                namespaces.Add(prefix, rootnamespace);
                xmlSerializer.Serialize(writer, @object, namespaces);
                xmlContent = sww.ToString();
            }

            return xmlContent;
         }
       
        public static T Deserialize<T>(string xmlContent)
        {
            try
            {
                var xmlSerializer = new XmlSerializer(typeof(T));

                using (var sr = new StringReader(xmlContent))
                    return (T)xmlSerializer.Deserialize(sr);
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}