﻿namespace CreditExchange.Syndication.Cibil.Response
{
    public interface ICibilReport
    {
        IReport Report { get; set; }
        string RequestXML { get; set; }
        string ResponseXML { get; set; }

       
    }
}
