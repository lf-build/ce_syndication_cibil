﻿using System.Linq;
using CreditExchange.Syndication.Cibil.CIR;
using CreditExchange.Syndication.Cibil.CreditBureau;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
#if DOTNET2
using System.Web;
#else
using RestSharp.Contrib;
#endif
using System;
using System.Collections.Generic;
using System.Security;
using LendFoundry.Foundation.Logging;

namespace CreditExchange.Syndication.Cibil.Response
{
    public class Report : IReport
    {

        public Report ()
        {

        }
        private ILogger Logger { get; set; }
        public Report (Proxy.Response.DCResponse response)
        {
            try
            {

                var applicaitondataText = response?.ContextData?.Field.Where (i => i.Key == "ApplicationData")?.FirstOrDefault ();
                if (!string.IsNullOrEmpty (applicaitondataText?.Text))
                {
                    var applicaitondataxmlstring = HttpUtility.HtmlDecode (SecurityElement.Escape (applicaitondataText?.Text));

                    var applicaitondata = XmlSerialization.Deserialize<Proxy.Response.ApplicationData> (applicaitondataxmlstring);
                    if (applicaitondata?.InputValReasonCodes?.Reason != null)
                    {
                        ValidationError = new Error (applicaitondata?.InputValReasonCodes?.Reason);
                    }
                    ApplicationId = applicaitondata?.ApplicationId;
                }
                var applicantsText = response?.ContextData?.Field.Where (i => i.Key == "Applicants")?.FirstOrDefault ();
                if (!string.IsNullOrEmpty (applicantsText?.Text))
                {
                    var applicantsxmlstring = HttpUtility.HtmlDecode (SecurityElement.Escape (applicantsText?.Text));
                    var applicants = XmlSerialization.Deserialize<Proxy.Response.Applicants> (applicantsxmlstring);
                    var bureauResponse = applicants?.Applicant?.DsCibilBureau?.Response?.CibilBureauResponse;
                    var riskBand = applicants?.Applicant?.DsTuNtc?.DsTuNtcStatus?.RiskBand;
                    var verificationXMLText = applicants?.Applicant?.DsTuVerification?.Response?.VerificationResponseXml;
                    if (!string.IsNullOrEmpty (verificationXMLText))
                    {
                        CibilReport = new CreditInformationBureauReport (XmlSerialization.Deserialize<Proxy.Response.Output> (verificationXMLText));
                    }

                    if (bureauResponse?.IsSucess?.ToUpper () == "False".ToUpper ())
                    {
                        ExceptionInfo = new Error (bureauResponse?.ErrorCode, bureauResponse?.ErrorMessage);
                    }
                    else
                    {
                        var creditreportxmlstring = HttpUtility.HtmlDecode (bureauResponse?.BureauResponseXml);
                        if (!string.IsNullOrEmpty (creditreportxmlstring))
                        {
                            try
                            {
                                CreditInfomationReport = new CreditReport (XmlSerialization.Deserialize<Proxy.Response.CibilReport> (creditreportxmlstring));

                            }
                            catch (InvalidOperationException ex)
                            {
                                ExceptionInfo = new Error (XmlSerialization.Deserialize<Proxy.Response.ErrorResponse> (creditreportxmlstring));
                            }
                        }
                    }
                    var additionalReportxmlstring = HttpUtility.HtmlDecode (bureauResponse?.SecondaryReportXml);
                    if (!string.IsNullOrEmpty (additionalReportxmlstring) && bureauResponse.IsSucess.ToUpper () == "TRUE")
                    {
                        var additionalreport = XmlSerialization.Deserialize<Proxy.Response.Root> (additionalReportxmlstring);
                        AdditionalFieldReport = additionalreport.CreditReport.Select (p => new CreditReport (p)).ToList<ICreditReport> ();
                    }
                    BureauResponseRaw = bureauResponse?.BureauResponseRaw;
                    RiskBand = riskBand;
                    var dsIDVision = applicants?.Applicant?.DsIDVision;
                    PanMatch = new Pan (dsIDVision?.CPVAttributes);

                }
                if (response?.ExceptionInfo != null)
                {
                    ExceptionInfo = new Error (response?.ExceptionInfo);
                }
            }
            catch (System.Exception ex)
            {
                // Logger.Info ("-=-=-=-=-=-Response here-=-=-=-=-=", response, true);
                throw new Exception ("exception from reponse", ex);
            }

        }
        public string ApplicationId { get; set; }

        [JsonConverter (typeof (InterfaceConverter<ICreditReport, CreditReport>))]
        public ICreditReport CreditInfomationReport { get; set; }

        [JsonConverter (typeof (InterfaceConverter<ICreditInformationBureauReport, CreditInformationBureauReport>))]
        public ICreditInformationBureauReport CibilReport { get; set; }

        [JsonConverter (typeof (InterfaceListConverter<ICreditReport, CreditReport>))]
        public List<ICreditReport> AdditionalFieldReport { get; set; }

        [JsonConverter (typeof (InterfaceConverter<IError, Error>))]
        public IError ValidationError { get; set; }

        [JsonConverter (typeof (InterfaceConverter<IError, Error>))]
        public IError ExceptionInfo { get; set; }

        [JsonConverter (typeof (InterfaceConverter<IPan, Pan>))]
        public IPan PanMatch { get; set; }
        public string BureauResponseRaw { get; set; }
        public string RiskBand { get; set; }
    }
}