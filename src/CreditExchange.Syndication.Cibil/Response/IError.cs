﻿namespace CreditExchange.Syndication.Cibil.Response
{
    public interface IError
    {
        string ErrorCode { get; set; }
        string ErrorDescription { get; set; }
    }
}
