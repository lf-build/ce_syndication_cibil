﻿using CreditExchange.Syndication.Cibil.CIR;
using CreditExchange.Syndication.Cibil.CreditBureau;

namespace CreditExchange.Syndication.Cibil.Response
{
    public interface IReport
    {
        string ApplicationId { get; set; }
        ICreditReport CreditInfomationReport { get; set; }
        ICreditInformationBureauReport CibilReport { get; set; }
        IError ValidationError { get; set; }
        IError ExceptionInfo { get; set; }
        IPan PanMatch { get; set; }
        string BureauResponseRaw { get; set; }
        string RiskBand { get; set; }
    }
}