﻿using LendFoundry.Security.Tokens;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
#else
using Microsoft.Framework.DependencyInjection;
#endif

namespace CreditExchange.Cibil.Client
{
    public static class CibilReportServiceClientExtension
    {
        public static IServiceCollection AddCibilReportService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<ICibilReportServiceClientFactory>(p => new CibilReportServiceClientFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<ICibilReportServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}
